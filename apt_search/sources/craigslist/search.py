#!/usr/bin/env python

# Custom script for Scraping apartments.com. 
import re
import urllib
import sys
import math
from apt_search.core.search import Search
from apt_search.sources.craigslist.unit import CraigslistUnit

class CraigslistSearch(Search):

    def loadItem(self, url):
        page = self.fetchPage(url)
        item = CraigslistUnit(page, url)
        return item 

    # Run apartment search. 
    def getItemUrls(self, page):
        
        units = page.find('ul', class_='rows')
        
        items = []
         # Loop through each individual unit
        for content in units.find_all('li', class_='result-row'):
            
            # If Unit has a link then follow it and parse that page data.
            if content.find('a', class_='result-image'):
                url = str(content.find('a', class_='result-image').get('href'))
                if url is not None:
                    items.append(url)
                
        return items

    # Get total number of results. 
    def getTotalResults(self, page):
        # Get the total number of results. 
        value = 0
        obj = page.find('span', class_="totalcount")
        if obj is not None:
            data = obj.getText()
            value = data.strip()
            value = re.sub("[^0-9|.]", "", data)
            
        if value is not None:
            self.totalResults = int(value)

        return self.totalResults

    def getNextPageNumber(self, page):
        next_link = page.find('a', class_='next')

        # If there's only one page this will actually be none
        if next_link is None:
            return
        
        # If on last page then stop processing pages 
        next_url = next_link.get('href')
        if next_url is None or next_url == '' or next_url == 'javascript:void(0)':
            return
                
        # Get the next page number. 
        split_url = urllib.parse.urlsplit(next_url)
        query = urllib.parse.parse_qs(split_url.query)

        iteration = 120
        if query.get('s') is None:
            page_number = iteration 
        else:
            value = query.get("s")
            current_page = int(value[0])
            page_number = current_page + iteration 

        return page_number

    # Generate pager URL. 
    def pagerUrl(self, number):
        split_url = urllib.parse.urlsplit(self.url)
        new_query = split_url.query + "&s=" + str(number)
        split_list = list(split_url)
        split_list[3] = new_query
        return urllib.parse.urlunsplit(split_list)

    # Get total number of pages. 
    def getTotalPages(self,page):
        self.totalPages = self.totalResults
        return self.totalPages

        
