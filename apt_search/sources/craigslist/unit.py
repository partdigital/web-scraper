#!/usr/bin/env python 

from apt_search.core.unit import Unit

class CraigslistUnit(Unit):

    def searchDescription(self, value):
        obj = self.unit.find('section', class_='postingBody')
        return self.matchText(obj, value)   
    
    # Get Url.
    def getUrl(self):
        return self.url
        
     # Get the title of the property. 
    def getTitle(self):
        obj = self.unit.find('span', {"id":'titletextonly'})
        if obj is not None:
            title = obj.getText()
            title = self.sanitize(title)
            return title
        
    # Get the rent of the property.
    def getRent(self):        
        obj = self.unit.find('span', class_='price')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getDeposit(self):        
        keywords = ["Broker Fee"]
        return self.searchDescription(keywords)
        
        
    def getBedrooms(self):        
        obj = self.unit.find('div', class_='mapAndAttrs')        
        keywords = ["1BR", "2BR"]
        return self.matchText(obj, keywords)
    
    def getBathrooms(self):        
        obj = self.unit.find('div', class_='mapAndAttrs')        
        keywords = ["1Ba", "2Ba"]
        return self.matchText(obj, keywords)
        
    def getSqFt(self): 
        obj = self.unit.find('td', class_='sqft')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getAvailability(self):        
        obj = self.unit.find('span', class_='property_date')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getUnitNum(self):        
        obj = self.unit.find('td', class_='unit')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getUnitSqft(self):        
        obj = self.unit.find('td', class_='sqft')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getLease(self):        
        obj = self.unit.find('td', class_='lease')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getPetPolicy(self):   
        obj = self.unit.find('div', class_='mapAndAttrs')        
        keywords = ["cats are OK"]
        return self.matchText(obj, keywords)
       
    def getOutdoor(self):
        # TODO: Ths is slow!
        keywords = ["porch", "deck", "balcony", "yard"]
        return self.searchDescription(keywords)
        
    def getParking(self):
        obj = self.unit.find('div', class_='mapAndAttrs')        
        keywords = ["off-street parking", "street parking", "parking"]
        return self.matchText(obj, keywords)
        
   