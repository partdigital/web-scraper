#!/usr/bin/env python

# Custom script for Scraping apartments.com. 
import re
import urllib
from apt_search.core.search import Search
from apt_search.sources.apartments.unit import AptUnit

class AptSearch(Search):

    def loadItem(self, url):
        page = self.fetchPage(url)
        item = AptUnit(page, url)
        return item 

    # Run apartment search. 
    def getItemUrls(self, page):
        
        units = page.find('div', class_='placardContainer')
        
        items = []
         # Loop through each individual unit
        for content in units.find_all('article', class_='placard'):
            
            # If Unit has a link then follow it and parse that page data.
            if content.find('a', class_='placardTitle'):
                url = str(content.find('a', class_='placardTitle').get('href'))
                if url is not None and url not in items:
                    items.append(url)
                
        return items

    # Get total number of results. 
    def getTotalResults(self, page):
        # Get the total number of results. 
        obj = page.find('h3', class_="count")
        if obj is not None:
            data = obj.getText()
            data = data.strip()
            value = re.findall("^([0-9]+) ", data)[0]
            
        if value is not None:
            self.totalResults = int(value)

        return self.totalResults

    def getNextPageNumber(self, page):
        next_link = page.find('a', class_='next')

        # If there's only one page this will actually be none
        if next_link is None:
            return
        
        # If on last page then stop processing pages 
        next_url = next_link.get('href')
        if next_url is None or next_url == '' or next_url == 'javascript:void(0)':
            return
                
        # Get the actual next URL address
        # We can't fetch it from the href because the href is not the full
        # valid URL. We need to regenerate it. 
        page_number = next_link.get('data-page')
        if page_number is None:
            return 
        
        return page_number

    # Generate pager URL. 
    def pagerUrl(self, number):
        split_url = urllib.parse.urlsplit(self.url)
        new_path = split_url.path + number + "/"
        split_list = list(split_url)
        split_list[2] = new_path
        return urllib.parse.urlunsplit(split_list)

    # Get total number of pages. 
    def getTotalPages(self,page):
        obj = page.find('a', class_="next")
        if obj is not None:
            sibling = obj.find_parent().find_previous_sibling().find("a")
            if sibling is not None:
                value = sibling.get("data-page")
                self.totalPages = value
            
        return self.totalPages
