#!/usr/bin/env python

# TODO: Add some delay so you're not scraping it too aggressively. 
# TODO: Remove duplicate results from list. 
import json
import re
import sys
import datetime
import requests
import os
import time 

import urllib

from selenium import webdriver  
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

from apt_search.core.unit import Unit
from apt_search.core.matrix import Matrix
from apt_search.utils.csv_helper import CsvHelper
from apt_search.utils.config import Config

class Search:

    # Class constants. 

    # Enable the javascript browser. 
    ENABLE_JS = False

    def __init__(self, url):

        # The Url argument. 
        self.url = url

        # Load settings from config. 
        self.config = Config().load()
        
         # The total number of results. 
        self.totalResults = 0
        
        # The total number of pages. 
        self.totalPages = 0
        
        # Default list of Fields that should be scraped. 
        self.defaultFields = ["Title", "Rent", "Bedrooms", "Bathrooms", "SqFt", "Availability", "Outdoor", "PetPolicy", "Parking", "Url"]  

        self.fields = self.getFields()
        
        # Set unique keys so that we don't have duplicates. 
        # TODO: There is a better way to to do this.
        self.keys = []

        # The processed scraped data. 
        self.data = []

    # Fetch the full page, unprocessed.  
    def fetchPage(self, url): 
        if self.ENABLE_JS:
            chrome_options = Options()  
            # chrome_options.add_argument("--headless")  
            chrome_options.binary_location = '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'    
            driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"),   chrome_options=chrome_options)  
            driver.get(url)
            driver.implicitly_wait(300)
            content = driver.page_source
        else:
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
            response = requests.get(url, headers=headers)
            content = response.content
        
        if content is not None:
            soup = BeautifulSoup(content, 'html.parser')
            soup.prettify()
            return soup
    
    # Fetch fields from config. 
    def getFields(self):
        fields = list(self.config.get("fields", []).keys())
        if fields is not None:
            return fields
        else:
            return self.defaultFields

    # Generate pager URL. 
    def pagerUrl(self, number):
        split_url = urllib.parse.urlsplit(self.url)
        new_path = split_url.path + number + "/"
        split_list = list(split_url)
        split_list[2] = new_path
        return urllib.parse.urlunsplit(split_list)

    # Run 
    def run(self):
              
        url = self.url
        
        print("Scraping: {}".format(url))

        # Fetch the page data. 
        page = self.fetchPage(url)

        # Fetch the total number of results.  . 
        self.getTotalResults(page)

        # Fetch the total number of pages. 
        self.getTotalPages(page)

        print("Total results found: {}".format(self.totalResults))

        self.doRun(page)
        
    # Recursive run. 
    def doRun(self, page):        
        
        # Parse page data. 
        # This will parse the page and return a list. 
        results = self.getItemUrls(page)

        # This will parse the self.items list. 
        self.parseItems(results)
        
        # get the next page URL for pagination
        page_number = self.getNextPageNumber(page)

        if page_number is not None:
            # Wait three seconds before fetching next page. 
            # This is help with any connection reset.
            time.sleep(3)

            print("Scraping page: {} of {}".format(page_number, self.totalPages))

            next_url = self.pagerUrl(page_number)

            page = self.fetchPage(next_url)  
            # recurse until the last page
            self.doRun(page)

    def parseItems(self, items):
        iterator = 0
        for url in items:
             # Fetch field data. 
            data = {}

            # After fetching 10 items, wait one second. 
            # this helps to slow down the scraping so that we don't get 
            # kicked off. 
            if iterator >= 10:
                time.sleep(1)
                iterator = 0
            
            Unit = self.loadItem(url)

            # TODO: Move this to Unit. 
            for field in self.fields:
                method = "get" + field
                if hasattr(Unit, method) and callable(getattr(Unit, method)):
                    method = getattr(Unit, method)
                    value = method()
                    if value is not None:
                        data[field] = value
                    else:
                        data[field] = ""
            if data:
                self.data.append(data)
            
            iterator = iterator+1 

    def loadItem(self, url):
        page = self.fetchPage(url)
        item = Unit(page, url)
        return item 

    # Run apartment search. 
    def getItemUrls(self, page): 
        items = []
        return items
    
    # Get total number of results. 
    def getTotalResults(self, page):
        return self.totalResults

    def getNextPageNumber(self, page):
        page_number = 1
        return page_number

    # Get total number of pages. 
    # Calculate and set the totalPages variable here. 
    def getTotalPages(self,page):
        return self.totalPages

        
