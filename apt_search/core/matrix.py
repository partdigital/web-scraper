#!/usr/bin/env python

import csv
import json
import re
import sys
import datetime
import requests
import os

import urllib
import statistics

from apt_search.utils.config import Config

# This class is used for calculating datatype scores. 
class Matrix:
    
    def __init__(self, data):
        # The scraped data. 
        self.data = data
        
        # The calculated scores for each row. 
        self.scores = []
        
        # Load settings from config. 
        self.config = Config().load()

    def calculateField(self, field, value):
        
        # We have to compare it with all the other column data in order to
        # get a relative score. 
        # TODO: No necessary for bool.    
        # TODO: This is expensive! Need to optimize it. 
        datatype = self.config.get("fields", {}).get(field)
 
        if datatype is not None: 
            column = []
            for row in self.data:
                if row[field]:
                    column.append(row[field])

            field_type = datatype.get("type")
            if field_type is not None:
                method = "calculate" + field_type
                if hasattr(self, method) and callable(getattr(self, method)):  
                    method = getattr(self, method)
                    op = datatype.get("op")
                    value = method(value, op, column)

                if value is not None:
                    weight = float(datatype.get('weight'))
                    if weight is not None:
                        # Todo: Scores are not accurate. 
                        score = round((weight * value)*100)                
                        return score
           
            # return value
    
        
    def calculateScores(self):

        for row in self.data:
            row_score = 0
            for key, value in row.items():
                score = self.calculateField(key, value)
                if score is not None:
                    row_score += score
    
            self.scores.append(row_score)
                
        
    # Calculate the score for a boolean. 
    def calculateBool(self, value, op, column):
        # TODO: Calculate boolean.
        if op == True:
            if value != "":
                return 1
        else:
            if value == "":
                return 1
            
        return 0 
        
    # Calculate the score for a number. 
    def calculateNumber(self, value, op, column):
        # Calculate score compared to the other values
        # in the column. 
        # If op is desc then give 1 to highest value. 
        
        # Make sure that values are integers
        value = re.sub("[^0-9|.]", "", value)
        
        # if value.isdigit == False:
        if value:
            value = int(value)

            # convert column values to integers. 
            # There must be a faster way. 
            int_column = []
            for col in column:
                int_column.append(int(re.sub("[^0-9|.]", "", col)))
             
            if value > 0:
                if op == "desc":
                    return (value / max(int_column))

                # Otherwise give 1 to the lowest value
                else: 
                    return (min(int_column) / value)

            return 0
   
    def calculateValue(self, value, op, column):
        if value == op:
            return 1
        else:
            return 0
        
    # Calculate the score for a date. 
    def calculateDate(self, value, op, column):
        return 0
        # TODO: This will be tricky. 