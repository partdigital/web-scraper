#!/usr/bin/env python

import csv
import json
import re
import sys
import datetime
import requests
import os
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from bs4 import BeautifulSoup

class Unit:
    # Class variables go here. Basically they are constants. 
    # Method must always have "self" 
    def __init__(self, unit, url):
        # Instance variables go here. Unique to each instance. 
        self.unit = unit
        
        # The Unit URL
        self.url = url
        
        # The scraped data. 
        self.data = []
        
        # Sanitize data. 
    def sanitize(self, data):
        
        data = re.sub(r'[^\x00-\x7F]+',' ', data)
        # Replace multiple spaces with just one
        data = re.sub(' +', ' ', data)
        # Replace multiple new lines with just one
        data = re.sub('(\r?\n *)+', '\n', data)
        # Replace bullet with *
        data = re.sub(u'\u2022', '* ', data)
        # Remove trailing spaces
        data = data.strip()
        # Encode it, removing special symbols
        # data = data.encode('utf8', 'ignore')
        
        return str(data)
        # return str(data).encode('utf-8')
     
    def matchText(self, element, value, single=None):
        # TODO: Ths is slow!
        matches = []
        s = " "
        obj = element
        if obj is not None:
            text = self.sanitize(obj.getText().lower())

            for word in value:
               # Using fuzzy search.
                ratio = fuzz.partial_ratio(text, word.lower())
                if ratio > 60:
                    result = {"text":word, "ratio":ratio }
                    matches.append(result)
                
        if len(matches) > 0:
            data = []
            # Sort data by ratio. 
            matches = sorted(matches, key=lambda i: i["ratio"], reverse=True)

            # If only a single result is desired them return that.  
            if single == True:
                data.append(matches[0].get("text"))
           
           # Otherwise return a list. 
            else:
                for item in matches:
                    data.append(item.get('text'))

            return s.join(data)

    def searchDescription(self, value):
        obj = self.unit.find('section', class_='descriptionSection')
        return self.matchText(obj, value)    
    
    # Get Url.
    def getUrl(self):
        return self.url
        
     # Get the title of the property. 
    def getTitle(self):
        obj = self.unit.find('h1', class_='propertyName')
        if obj is not None:
            title = obj.getText()
            title = self.sanitize(title)
            return title
        
    # Get the rent of the property.
    def getRent(self):        
        obj = self.unit.find('td', class_='rent')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getDeposit(self):        
        obj = self.unit.find('td', class_='deposit')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data     
        
    def getBedrooms(self):        
        obj = self.unit.find('tr', {'data-beds': True})
        if obj is not None:
            return self.sanitize(obj['data-beds'])
    
    def getBathrooms(self):        
        obj = self.unit.find('tr', {'data-baths': True})
        if obj is not None:
            return self.sanitize(obj['data-baths'])
        
    def getSqFt(self): 
        obj = self.unit.find('td', class_='sqft')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getAvailability(self):        
        obj = self.unit.find('td', class_='available')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getUnitNum(self):        
        obj = self.unit.find('td', class_='unit')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getUnitSqft(self):        
        obj = self.unit.find('td', class_='sqft')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getLease(self):        
        obj = self.unit.find('td', class_='lease')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getPetPolicy(self):   
        obj = self.unit.find('div', class_='petPolicyDetails')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
       
    def getOutdoor(self):
        # TODO: Ths is slow!
        keywords = ["porch", "deck", "balcony", "yard"]
        return self.searchDescription(keywords)
        
    def getParking(self):
        obj = self.unit.find('div', class_='parkingDetails')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
        obj = self.unit.find('section', class_='descriptionSection')
        keywords = ["parking for rent", "parking for a fee", "parking off-street", "parking space included"]
        return self.matchText(obj, keywords, True)    

   