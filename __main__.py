#!/usr/bin/env python
import sys
import os

from apt_search.core.search_factory import SearchFactory
from apt_search.core.matrix import Matrix
from apt_search.utils.csv_helper import CsvHelper

# Pass arguments
# Source can be a URL or path to csv file. 
source = sys.argv[1]

# Destinatin is path to csv file. 
destination = sys.argv[2]

# Source is file, pull from that instead. 
# TODO: Come back to this. 
if os.path.exists(source):
    csv = CsvHelper()
    sources = [line.rstrip('\n') for line in open(source)]

# Otherwise assume that argument is a URL. 
else:
    sources = [source]

# TODO: Remove this later. 
if len(sys.argv) > 1 and sys.argv[1] == "test":
    aptSearch = SearchFactory.load(destination)
else:
    # Run the script and append the results to the CSV file. 
    for url in sources:
        # Sanitize the URL. 
        url_sanitized = str(url).strip()

        # Load and run the search service. 
        aptSearch = SearchFactory.load(url_sanitized)
        aptSearch.run()

        # Write the results to a file. 
        # TODO: This is kind of ugly. 
        csv = CsvHelper()
        csv.data = aptSearch.data
        if sources.index(url) == 0:
            csv.write(destination) 
        else: 
            csv.append(destination)


# After data is collected, remove duplicates and
# calculate the matrix scores.
csv = CsvHelper()
data = csv.read(destination)
# Remove any duplicates from the data. 
# TODO: This is not ideal. 
data = [dict(t) for t in {tuple(d.items()) for d in data}]

calculator = Matrix(data)
calculator.calculateScores()

# Append scores to end of csv file. 
for i, row in enumerate(data):
    score = calculator.scores[i]
    row["Score"] = score

# Sort the data by score from highest to lowest and save it. 
data = sorted(data, key=lambda i: i["Score"], reverse=True)

# Override the data and save it to CSV. 
# Have to append score to the fields list. 
csv = CsvHelper()
csv.data = data
csv.write(destination)